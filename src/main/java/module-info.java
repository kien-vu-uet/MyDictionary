module com.project1 {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.media;
    requires javafx.base;
    requires javafx.web;
    requires javafx.swing;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;
    requires freetts;
    requires sqlite.jdbc;

    opens com.project1 to javafx.fxml;
    exports com.project1;
    exports com.project1.controllers;
    opens com.project1.controllers to javafx.fxml;
}