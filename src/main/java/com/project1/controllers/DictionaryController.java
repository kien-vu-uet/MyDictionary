package com.project1.controllers;

import com.project1.Main;
import com.project1.sources.SQLiteJDBC;
import com.project1.sources.TextToSpeech;
import com.project1.sources.Word;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class DictionaryController {
    @FXML
    private Button returnHomeButton;

    @FXML
    private Button searchButton;

    @FXML
    private TextField wordTypingBar;

    @FXML
    private ListView<Text> wordArea;

    @FXML
    private TextArea pronunciationArea;

    @FXML
    private TextArea meaningArea;

    @FXML
    private Button pronounceButton;

    @FXML
    private Label messageArea;

    @FXML
    private Button deleteButton;

    @FXML
    private Button modifyButton;

    @FXML
    private Stage stage;

    @FXML
    private static Stage modifyStage;

    private static Word wordFound;

    @FXML
    private void returnHomeButtonOnClick() throws Exception{
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("fxml/Home.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 720, 480);
            this.stage = Main.getMainStage();
            stage.setTitle("My Dictionary");
            stage.getIcons().add(new Image(Objects.requireNonNull(Main.class.getResourceAsStream("icon/icon.png"))));
            stage.setScene(scene);
            this.stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void searchButtonOnClick() throws Exception {
        wordArea.getItems().clear();
        pronunciationArea.setText("");
        meaningArea.setText("");
        String input = wordTypingBar.getText();
        if (input.equals("")) {
            messageArea.setText("Nothing to search...");
        } else messageArea.setText("");
        List<Word> relatedWord = SQLiteJDBC.patternSearch(input);
        if (relatedWord.isEmpty()) {
            messageArea.setText("No result found!");
            return;
        } else {
            Font font = new Font(18);
            for (Word word : relatedWord) {
                Text text = new Text(word.getTarget());
                text.setFont(font);
                text.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        if (mouseEvent.getSource() == text) {
                            wordFound = SQLiteJDBC.queryLookup(word.getId());
                            wordTypingBar.setText(wordFound.getTarget());
                            pronunciationArea.setText(wordFound.getPronounce());
                            meaningArea.setText(wordFound.getExplain());
                        }
                    }
                });
                wordArea.getItems().add(text);
            }
        }
    }

    @FXML
    private void pronounceButtonOnClick() {
        if (wordFound == null) return;
        TextToSpeech.speak(wordFound.getTarget());
    }

    @FXML
    private void deleteButtonOnclick() throws Exception {
        if (wordFound == null) {
            return;
        }
        SQLiteJDBC.deleteRowWithKey(wordFound.getId());
    }

    @FXML
    private void modifyButtonOnclick() throws Exception {
        if (wordFound == null) {
            return;
        }
        createDialog(0);
    }

    public static void createDialog(int option) {
        String title;
        if (option == 0) title = "Modification";
        else title = "Addition";
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        File iconFile = new File("src/main/resources/com/uet/dbms/icons/icon.png");
        stage.getIcons().add(new Image(iconFile.toURI().toString()));
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                if (windowEvent.getSource() == stage) {
                    System.out.println("close window");
                    stage.close();
                }
            }
        });


        Font font = new Font(18);

        TextField wordArea = new TextField();
        if (option == 0) wordArea.setText(wordFound.getTarget());
        else if (option == 1) wordArea.setPromptText("Word");
        wordArea.setFont(font);
        wordArea.setPrefWidth(310);
        wordArea.setPrefHeight(40);
        wordArea.setLayoutX(60);
        wordArea.setLayoutY(55);

        TextField pronounceArea = new TextField();
        if (option == 0) pronounceArea.setText(wordFound.getPronounce());
        else if (option == 1) pronounceArea.setPromptText("Pronunciation");
        pronounceArea.setFont(font);
        pronounceArea.setPrefWidth(155);
        pronounceArea.setPrefHeight(40);
        pronounceArea.setLayoutX(385);
        pronounceArea.setLayoutY(55);

        TextArea descriptionArea = new TextArea();
        if (option == 0) descriptionArea.setText(wordFound.getExplain());
        else if (option == 1) descriptionArea.setPromptText("Description");
        descriptionArea.setFont(font);
        descriptionArea.setPrefWidth(480);
        descriptionArea.setPrefHeight(200);
        descriptionArea.setLayoutX(60);
        descriptionArea.setLayoutY(115);
        
        Text text = new Text();
        text.setLayoutX(60);
        text.setLayoutY(30);

        Button saveButton = new Button("Save");
        saveButton.setPrefWidth(160);
        saveButton.setPrefHeight(40);
        saveButton.setLayoutX(220);
        saveButton.setLayoutY(335);
        saveButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getSource() == saveButton) {
                    String newTarget = wordArea.getText();
                    String newPronounce = pronounceArea.getText();
                    String newDescription = descriptionArea.getText();
                    if (option == 0) {
                        if (newTarget.equals(wordFound.getTarget())
                                && newDescription.equals(wordFound.getExplain())
                                && newPronounce.equals(wordFound.getPronounce())) {

                        } else {
                            Word modWord = new Word(wordFound.getId(), newTarget, newDescription, newPronounce);
                            SQLiteJDBC.modifyDatabase(modWord);
                        }
                    } else if (option == 1) {
                        if (newTarget.isEmpty() && newDescription.isEmpty()) {

                        } else {
                            Word newWord = new Word(newTarget, newDescription, newPronounce);
                            SQLiteJDBC.insertToDatabase(newWord);
                        }
                    }
                    text.setText("Successfully");
                }
            }
        });

        Pane pane = new Pane();
        pane.setPrefWidth(600);
        pane.setPrefHeight(400);
        pane.getChildren().addAll(wordArea, pronounceArea, descriptionArea, saveButton, text);

        DialogPane dialogPane = new DialogPane();
        dialogPane.setContent(pane);
        dialog.setDialogPane(dialogPane);
        dialog.showAndWait();
    }
}
