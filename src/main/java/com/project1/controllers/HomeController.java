package com.project1.controllers;

import com.project1.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Objects;

public class HomeController {
    @FXML
    private Button dictionaryButton;

    @FXML
    private Button textTranslationButton;

    @FXML
    private Button newWordButton;

    @FXML
    private void buttonOnClick(ActionEvent event) {
        if (event.getSource() == dictionaryButton){
            loadStage("fxml/Dictionary.fxml", 720, 480);
        }
        else if (event.getSource() == textTranslationButton) {
            loadStage("fxml/TextTranslation.fxml", 1080, 720);
        }
        else if (event.getSource() == newWordButton) {
            DictionaryController.createDialog(1);
        }
    }

    private void loadStage(String path, int width, int height) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(path));
            Scene scene = new Scene(fxmlLoader.load(), width, height);
            Stage stage = Main.getMainStage();
            stage.setTitle("My Dictionary");
            stage.getIcons().add(new Image(Objects.requireNonNull(Main.class.getResourceAsStream("icon/icon.png"))));
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}