package com.project1.controllers;

import com.project1.Main;
import com.project1.sources.GG_API_Translator;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class TextTranslationController {
    @FXML
    private Button returnHomeButton;

    @FXML
    private Button translateButton;

    @FXML
    private TextArea textArea;

    @FXML
    private TextArea translatedTextArea;

    @FXML
    private Label messageLabel;

    @FXML
    private Stage stage;

    @FXML
    private void returnHomeButtonOnClick() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("fxml/Home.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 720, 480);
            this.stage = Main.getMainStage();
            stage.setTitle("My Dictionary");
            stage.getIcons().add(new Image(Objects.requireNonNull(Main.class.getResourceAsStream("icon/icon.png"))));
            stage.setScene(scene);
            this.stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void translateButtonOnClick() throws IOException {
        String text = textArea.getText();
        if (text.equals("")) {
            messageLabel.setText("Đoạn văn của bạn đang trống...");
            return;
        }
        text = text + "\n";
        String result = "";
        while (text.contains("\n")) {
            int id = text.indexOf("\n");
            String paragraph = text.substring(0, id);
            result += GG_API_Translator.translate("en", "vi", paragraph) + "\n";
            StringBuilder temp = new StringBuilder(text);
            temp.replace(0, id + 1, "");
            text = temp.toString();
            if (text.equals("\n")) break;
        }
        translatedTextArea.setText(result);
        messageLabel.setText("Dịch thành công!");
        this.stage = Main.getMainStage();
        this.stage.show();//
    }
}
