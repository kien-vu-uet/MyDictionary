package com.project1.sources;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLiteJDBC {

    public static void insertToDatabase(Word w) {
        Connection connection = null;
        Statement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:dictionary.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            String command = "INSERT INTO EngToVie (word,description,pronounce) " +
                    "VALUES ('" + w.getTarget() +"','" + w.getExplain() + "', \"" +
                    w.getPronounce() + "\");";
            statement.executeUpdate(command);
            statement.close();
            connection.commit();
            connection.close();
        } catch (Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }

    public static Word queryLookup(int id) {
        Word word = null;
        Connection connection = null;
        Statement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:dictionary.db");
            if (connection == null) {
                System.out.println("No connect");
            }
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM EngToVie Where id = " + id + ";" );
            String w = rs.getString("word");
            String p = rs.getString("pronounce");
            String d = rs.getString("description");
            word = new Word(id, w, d, p);
            rs.close();
            statement.close();
            connection.close();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        }
        return word;
    }

    public static List<Word> patternSearch(String pat) throws IOException {
        List<Word> res = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:dictionary.db");
            if (connection == null) {
                System.out.println("No connect");
            }
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM EngToVie Where word LIKE '" + pat + "%' ORDER BY word LIMIT 30;");
            while (rs.next()) {
                int id = rs.getInt("id");
                String w = rs.getString("word");
                String d = rs.getString("description");
                String p = rs.getString("pronounce");
                res.add(new Word(id, w, d, p));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println("No result found");
        }
        return res;
    }

    public static void modifyDatabase(Word word) {
//        @SuppressWarnings("unused")
//        String catchEnter = sc.nextLine();
        Connection connection = null;
        Statement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:dictionary.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate("UPDATE EngToVie SET word = '" + word.getTarget() + "' WHERE id ='" + word.getId() + "';");
            statement.close();
            connection.commit();

            statement = connection.createStatement();
            statement.executeUpdate("UPDATE EngToVie SET description = '" + word.getExplain() + "' WHERE id ='" + word.getId() + "';");
            statement.close();
            connection.commit();

            statement = connection.createStatement();
            statement.executeUpdate("UPDATE EngToVie SET pronounce = \"" + word.getPronounce() + "\" WHERE id ='" + word.getId() + "';");
            statement.close();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Edit done Successful");
    }

    public static void deleteRowWithKey(int id) throws IOException {
        Connection connection = null;
        Statement statement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:dictionary.db");
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM EngToVie WHERE id = " + id + ";");
            statement.close();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            System.err.println("No Word Found, Escaped Method!");
        }
    }
}
