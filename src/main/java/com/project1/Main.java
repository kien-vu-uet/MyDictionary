package com.project1;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Objects;

public class Main extends Application {

    @FXML
    private static Stage mainStage;

    @Override
    public void start(Stage stage) throws RuntimeException {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("fxml/Home.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 720, 480);
            stage.setTitle("My Dictionary");
            stage.getIcons().add(new Image(Objects.requireNonNull(Main.class.getResourceAsStream("icon/icon.png"))));
            stage.setScene(scene);
            mainStage = stage;
            stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static Stage getMainStage() {
        return mainStage;
    }

    public static void main(String[] args) throws RuntimeException {
        launch(args);
    }
}